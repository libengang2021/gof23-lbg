package com.lbg.gof23.singleton;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author libengang
 * @date 2021/11/18 9:16
 */
public class Singleton01Test {

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSingleton() {
        Singleton01 instance = Singleton01.getInstance();
        Assert.assertNotNull(instance);
        assertEquals(instance, Singleton01.getInstance());

    }
}