package com.lbg.gof23.singleton;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author libengang
 * @date 2021/11/19 9:09
 */
public class Singleton02Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test1() {
        Singleton02 singleton02 = Singleton02.getInstance();
        for (int i = 0; i < 100; i++) {
            System.out.println(i);
            assertEquals(singleton02, Singleton02.getInstance());

        }
    }
}