package com.lbg.gof23.singleton;

/**
 * @author libengang
 * @date 2021/11/18 9:22
 */
public class Singleton02 {
    // 懒汉式：调用getInstance函数时才会加载对象
    // 优点：保证不会浪费内存资源（空间），如直接调用类的静态方法不会创建单例对象
    private static Singleton02 singleton02;

    private Singleton02() {

    }

    public static synchronized Singleton02 getInstance() {
        if (singleton02 == null) {
            singleton02 = new Singleton02();
        }
        return singleton02;
    }

}
